import org.junit.jupiter.api.Test;
import static io.restassured.RestAssured.when;

public class API {

    @Test
    public void TestAPI(){

        String URL = "https://oz.by/";
        when().get(URL).
                then().
                assertThat().
                statusCode(200);
    }
}
