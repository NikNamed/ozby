import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Pages {

    WebDriver driver;
    @BeforeEach
    public void startTEST() {

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(chromeOptions);
        driver.manage().window().maximize();
        driver.get(TEST.BASE_URL);}
    @Test
    @DisplayName("Авторизация с корректными данными")
    public void testLogCorForm() {

        driver.findElement(By.xpath(TEST.BTN_ENTER)).click();
        TIME.waitFor(1);
        driver.findElement(By.xpath(TEST.LOGIN_FORM_LINK)).click();
        driver.findElement(By.xpath(TEST.INPUT_LOG)).sendKeys("123456789kaka12@mail.ru");
        driver.findElement(By.xpath(TEST.INPUT_PSW)).sendKeys("T8Yk7U");
        driver.findElement(By.xpath(TEST.BTN_LOG)).click();
        TIME.waitFor(1);
        driver.findElement(By.xpath(TEST.SEARCH_PANEL)).sendKeys("Васильев");
        driver.findElement(By.xpath(TEST.SEARCH_BTN)).click();
        TIME.waitFor(2);
        driver.findElement(By.xpath(TEST.ITEM_BOOK)).click();}
    @Test
    @DisplayName("Авторизация с некорректными данными")
    public void testLogUnСorForm() {

        driver.findElement(By.xpath(TEST.BTN_ENTER)).click();
        TIME.waitFor(2);
        driver.findElement(By.xpath(TEST.LOGIN_FORM_LINK)).click();
        driver.findElement(By.xpath(TEST.INPUT_LOG)).sendKeys("test@mail.ru");
        driver.findElement(By.xpath(TEST.INPUT_PSW)).sendKeys("501QWE");
        driver.findElement(By.xpath(TEST.BTN_LOG)).click();
        driver.findElement(By.xpath(TEST.DIV_TEXT)).getText();}
    @Test
    @DisplayName("Авторизация с корректным логином")
    public  void testLogCorLog(){

        driver.findElement(By.xpath(TEST.BTN_ENTER)).click();
        TIME.waitFor(2);
        driver.findElement(By.xpath(TEST.LOGIN_FORM_LINK)).click();
        driver.findElement(By.xpath(TEST.INPUT_LOG)).sendKeys("123456789kaka12@mail.ru");
        driver.findElement(By.xpath(TEST.INPUT_PSW)).sendKeys("501QWE");
        driver.findElement(By.xpath(TEST.BTN_LOG)).click();
        driver.findElement(By.xpath(TEST.DIV_TEXT)).getText();}
    @Test
    @DisplayName("Авторизация с корректным паролем")
    public  void testLogCorPas() {

        driver.findElement(By.xpath(TEST.BTN_ENTER)).click();
        TIME.waitFor(2);
        driver.findElement(By.xpath(TEST.LOGIN_FORM_LINK)).click();
        driver.findElement(By.xpath(TEST.INPUT_LOG)).sendKeys("test@mail.ru");
        driver.findElement(By.xpath(TEST.INPUT_PSW)).sendKeys("T8Yk7U");
        driver.findElement(By.xpath(TEST.BTN_LOG)).click();
        driver.findElement(By.xpath(TEST.DIV_TEXT)).getText();}
    @AfterEach

    public void EndTest() {driver.quit();}}