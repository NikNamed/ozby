public class TIME {

    public static void waitFor(int seconds) {
        try {
            Thread.sleep(seconds * 2000);

        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}