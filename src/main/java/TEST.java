public class TEST {

    static final String BASE_URL = " https://oz.by/";

    static final String BTN_ENTER = " /html/body/div[1]/div/div[1]/div[2]/div/ul/li[2]/a/span[1]";

    static final String LOGIN_FORM_LINK = " /html/body/div[5]/div/div/div/div[1]/div[2]/ul/li[1]/a";

    static final String INPUT_LOG = " /html/body/div[5]/div/div/div/div[1]/div[3]/div/div[1]/form/div[2]/div[1]/div[1]/input";

    static final String INPUT_PSW = " /html/body/div[5]/div/div/div/div[1]/div[3]/div/div[1]/form/div[2]/div[1]/div[2]/input";

    static final String DIV_TEXT = " /html/body/div[1]/div/div/div[1]/div/div/div[1]/div[3]/div/div[1]/form/div[2]/div[2]/div";

    static final String BTN_LOG = " /html/body/div[5]/div/div/div/div[1]/div[3]/div/div[1]/form/button";

    static final String SEARCH_PANEL = " /html/body/div[1]/div/div[1]/div[2]/div/form/div/input[2]";

    static final String SEARCH_BTN = " /html/body/div[1]/div/div[1]/div[2]/div/form/div";

    static final String  ITEM_BOOK = " /html/body/div[1]/div/div[3]/div/div/div[2]/div/div[2]/ul/li[4]";
}
